CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Provides a download handler for videos hosted on Vimeo for the
media_entity_download module.

INSTALLATION
------------

Install the module as usual, except if you are running Drupal 9 on PHP 8.

The Vimeo API has a dependency on ankitpokhrel/tus-php, which requires
components from Symfony 6 while Drupal 9 is still dependent on Symphony 5.

As a workaround you can use a fork of the Vimeo API which has the upload
functionality removed and does not have the Symfony 6 dependency:

You can replace the official vimeo-api in your global composer.json:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://github.com/Nebel54/vimeo.php"
    }
],
"replace": {
    "vimeo/vimeo-api": "2.*"
},
```

CONFIGURATION
-------------

The module requires a valid Vimeo Pro account to generate download links for
vimeo videos. As a prerequisite a vimeo APP needs to be created.

* Vimeo backend to create new APPs:
  https://developer.vimeo.com/apps
* Documentation of the Vimeo Authentication process:
  https://developer.vimeo.com/api/authentication


The module does not have a graphical User interface for the credential
configuration. To set the credentials, add the following configuration to
your `settings.local.php`:

```
$config['media_entity_download_vimeo.settings']['client_id'] = '';
$config['media_entity_download_vimeo.settings']['client_secret'] = '';
$config['media_entity_download_vimeo.settings']['access_token'] = '';
```

You can also use drush `drush cset media_entity_download_vimeo.settings` for
the settings:

* client_id
* client_secret
* access_token

MAINTAINERS:
------------

* Oliver Koehler - https://www.drupal.org/u/nebel54
